<?php

final class Products
{
    /**
     * @var string
     */
    private $serverName = 'localhost';

    /**
     * @var string
     */
    private $username = 'root';

    /**
     * @var string
     */
    private $password = 'root';

    /**
     * @var PDO
     */
    private $db;

    /**
     * @var string
     */
    private $products = '';

    public function __construct()
    {
        $this->connectionDB();
    }

    /**
     * @return $this
     */
    private function connectionDB()
    {
        try {
            $dsn = sprintf('mysql:host=%s;dbname=online_school;charset=utf8', $this->serverName);
            $db = new PDO($dsn, $this->username, $this->password);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->db = $db;
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }

        return $this;
    }

    /**
     * @return array
     */
    private function receiveProducts()
    {
        $products = $this->db->prepare("SELECT * FROM products");
        $products->execute();
        $products->setFetchMode(PDO::FETCH_ASSOC);

        return $products->fetchAll();
    }

    /**
     * @return array
     */
    private function sortProducts()
    {
        $list = [];

        foreach ($this->receiveProducts() as $product) {
            if (empty($list[$product['parent_id']])) {
                $list[$product['parent_id']] = [];
            }

            $list[$product['parent_id']][] = $product;
        }

        return $list;
    }

    /**
     * @param int $parentId
     * @return string
     */
    public function run($parentId = 0)
    {
        $products = $this->sortProducts();

        if (empty($products[$parentId])) {
            $this->products .= '</li>';

            return '';
        }

        $this->products .= '<ul>';

        foreach ($products[$parentId] as $product) {
            $this->products = sprintf('%s  <li> %s ', $this->products, $product['title']);
            $this->run($product['id']);

            $this->products .= '</li>';
        }

        $this->products .= '</ul>';

        return $this->products;
    }
}

$products = new Products();
echo $products->run();
